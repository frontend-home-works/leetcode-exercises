//  https://leetcode.com/problems/palindrome-number/

/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome1 = function (x) {
  // Stringə çeviririk ki, daha sonra massivə çevirə bilək
  let str = x.toString();
  // Massiv alandan sonra reverse() istifadə etmək mümkündür
  const arr = str.split('');
  // Əgər reverse və adi halı eynidirsə polindromdur
  if (arr.reverse().join('') === str) {
    return true;
  }
  return false;
};

/**
 * @param {number} x
 * @return {boolean}
 */
// Daha az kodla almaq istədim, amma nəticədə bu kodun exec. time daha yuxarı oldu
var isPalindrome2 = function (x) {
  let str = x.toString();
  // String-ə, daha sonra split ilə massivə çevirib,
  // reverse edib, yenidən join-lə stringə çevirib, original ilə tutuşdururam
  if (str.split('').reverse().join('') === str) return true;
  return false;
};
