// https://leetcode.com/problems/single-number/

/**
 * @param {number[]} arr
 * @return {number}
 */
var singleNumber = function (arr) {
  //  Massivi sort edək
  const tmpArr = arr.sort();
  for (let i = 0; i < tmpArr.length; i++) {
    //  Əgər hazırki ədədin sağında və solunda təkrarı yoxdursa, deməli tək ədədi tapmışıq
    //  Nəzərə almaq lazımdır ki, leetcode-da şərtdə deyilmişdi ki, ancaq 1 ədəd təkrarlanmayacaq.
    // Əgər Java olsaydı, burada index out of range alardıq, amma js-də undefined alırıq deyə, rahat istifadə etdim
    if (tmpArr[i] != tmpArr[i + 1] && tmpArr[i] != tmpArr[i - 1]) {
      return tmpArr[i];
    }
  }
  return NaN;
};
