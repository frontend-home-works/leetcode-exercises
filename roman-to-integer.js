// https://leetcode.com/problems/roman-to-integer/

// Metodu roman.convert(sym) kimi də işə salmaq olar.
// Leetcode-a görə digər funksiyanın içində çağırdım
const roman = {
  I: 1,
  IV: 4,
  V: 5,
  IX: 9,
  X: 10,
  XL: 40,
  L: 50,
  XC: 90,
  C: 100,
  D: 500,
  CD: 400,
  CM: 900,
  M: 1000,
  convert: function (sym) {
    // Bura son nəticəni yığacam
    let tmp = 0;
    for (let i = 0; i < sym.length; i++) {
      // 2 simvollu ədədləri tapmaq üçün ilk öncə 2 yanaşı ədədi yoxlayaq
      if (sym[i] + sym[i + 1] in this) {
        // Tapıldığı halda i-nin qiymətini də artıraq ki, cütlük simvol daha yoxlanmasın
        // Son nəticənin üstünə gələk
        tmp += this[sym[i] + sym[++i]];
        // Cüt simvollarda tapılmasa, tək simvolları axtaraq
      } else if (sym[i] in this) {
        tmp += this[sym[i]];
      }
    }
    return tmp;
  },
};
/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function (s) {
  return roman.convert(s);
};
