// https://leetcode.com/problems/length-of-last-word/

var lengthOfLastWord = function (s) {
  /*
  Son hissəni trim edirəm, sonra boşluğa görə split edib, əksinə çevirəndə,
  boşluq varsa, sözün əvvəlindən trimStart() ilə silinəcək və ilk index-də
  sonuncu söz olacaq.

  Ona görə reverse etdim ki, massivin sonuncu index-ini tapmağa ehtiyac olmasın.
  */
  return s.trimEnd().split(' ').reverse()[0].trimStart().length;
};
