// https://leetcode.com/problems/implement-strstr/

/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function (haystack, needle) {
  if (needle.length === 0) return 0;
  // We want to avoid indexOf, that why I choose this way
  for (let i = 0; i < haystack.length; i++) {
    if (haystack.slice(i).startsWith(needle)) {
      return i;
    }
  }
  return -1;
};
